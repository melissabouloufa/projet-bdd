
DO $$
BEGIN
    BEGIN
        DEALLOCATE reco_film;
    EXCEPTION
        WHEN others THEN
            NULL;
    END;
END$$;

PREPARE reco_film(INTEGER) AS
-- valeurs d'importance
WITH constantes(EC_value, FC_value, RC_value, REP_value, CC_value, FOC_value) as (
    select 3 as EC_value, 4 as FC_value, 2 as RC_value, 1 as REP_value, 3.5 as CC_value, 2 as FOC_value 
),


-- evenemnts en commun
event_communs(utilisateur, points_communs) AS(
    SELECT pid as utilisateur, COUNT(eid) AS points_communs
    FROM interet
    WHERE pid <> $1 
    AND eid IN ( 
        SELECT E.eid
        FROM evenement E
        NATURAL JOIN interet 
        WHERE pid = $1
    )
    GROUP BY pid
    ORDER BY points_communs DESC
),
-- films en commun
films_communs(utilisateur, points_communs) AS(
    SELECT uid as utilisateur, COUNT(fid) AS points_communs
    FROM historique
    WHERE uid <> $1 
    AND fid IN ( 
        SELECT H.fid
        FROM historique H
        WHERE uid = $1
    )
    GROUP BY uid
    ORDER BY points_communs DESC
),

-- reactions à des publications en commun
reaction_communes(utilisateur, points_communs) AS(
    SELECT R2.uid as utilisateur, COUNT(R1.uid) AS points_communs
    FROM reaction R1
    JOIN reaction R2 ON R1.pubid = R2.pubid
    WHERE R1.uid = $1
    AND R2.uid <> R1.uid
    AND (R1.emoji = R2.emoji
        OR ((R1.emoji = 'degout' OR R1.emoji = 'jaime_pas' OR R1.emoji = 'colere') 
            AND (R2.emoji = 'degout' OR R2.emoji = 'jaime_pas' OR R2.emoji = 'colere'))
        OR ((R1.emoji = 'jaime' OR R1.emoji = 'coeur' OR R1.emoji = 'joie') 
            AND (R2.emoji = 'jaime' OR R2.emoji = 'coeur' OR R2.emoji = 'joie'))
        OR ((R1.emoji = 'peur' OR R1.emoji = 'tristesse') 
            AND (R2.emoji = 'peur' OR R2.emoji = 'tristesse'))
        )
    GROUP BY R2.uid
    ORDER BY points_communs DESC
),

-- reponses à des publications en commun 
reponses_auteurs AS (
    SELECT P1.auteur AS id_auteur, R1.pubid AS id_pub, R1.pubid AS id_rep
    FROM publication P1
    JOIN reponse R1 ON R1.repid = P1.pubid
),

reponses_communes(utilisateur, points_communs) AS(
    SELECT RA2.id_auteur as utilisateur, COUNT(RA1.id_auteur) AS points_communs
    FROM reponses_auteurs RA1
    JOIN reponses_auteurs RA2 ON RA1.id_pub = RA2.id_pub
    WHERE RA1.id_auteur = $1
    AND RA1.id_auteur <> RA2.id_auteur
    GROUP BY RA2.id_auteur
),

-- club en commun 
clubs_communs(utilisateur, points_communs) AS(
    SELECT m2.pid as utilisateur, COUNT(m2.oid) AS points_communs
    FROM membre_club m1 join membre_club m2 on m1.oid=m2.oid
    WHERE m1.pid = $1 and m2.pid<>m1.pid
    GROUP BY m2.pid
),

-- follow en commun 
follow_communs(utilisateur, points_communs) AS(
    SELECT m2.suiveur as utilisateur, COUNT(m2.suivi) AS points_communs
    FROM suivi m1 join suivi m2 on m1.suivi=m2.suivi
    WHERE m1.suiveur = $1 and m2.suiveur<>m1.suiveur
    GROUP BY m2.suiveur
),

profils_similaires_tmp(utilisateur, points_communs) AS(
    select utilisateur, points_communs*EC_value as points_communs from event_communs, constantes
    UNION ALL
    select utilisateur, points_communs*FC_value as points_communs from films_communs, constantes
    UNION ALL
    select utilisateur, points_communs*RC_value as points_communs from reaction_communes, constantes
    UNION ALL
    select utilisateur, points_communs*REP_value as points_communs from reponses_communes, constantes
    UNION ALL
    select utilisateur, points_communs*CC_value as points_communs from clubs_communs, constantes
    UNION ALL
    select utilisateur, points_communs*FOC_value as points_communs from follow_communs, constantes
),

profils_similaires(utilisateur, points_communs) AS(
    select utilisateur, SUM(points_communs) as total_pc
    FROM profils_similaires_tmp
    GROUP BY utilisateur
    ORDER BY total_pc DESC
),

film_reco(fid,indice_reco) AS(
    select fid, SUM(points_communs) as indice_reco
    from (
        select * from historique where fid not in (
            select fid from historique where uid=$1
        )
    ) as histo join profils_similaires on utilisateur=uid
    group by fid
)

select e.titre, r.indice_reco 
from film_reco r natural join film e
ORDER BY r.indice_reco DESC
LIMIT 5;