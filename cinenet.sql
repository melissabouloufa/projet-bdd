drop table if exists contient;
drop table if exists mots_cle;
drop table if exists reaction;
drop table if exists reponse;
drop table if exists publication;
drop table if exists interet;
drop table if exists programme;
drop table if exists even_passe;
drop table if exists evenement;
drop table if exists casting;
drop table if exists realise;
drop table if exists produit;
drop table if exists membre_club;
drop table if exists seance;
drop table if exists historique;
drop table if exists film;
drop table if exists sous_genre;
drop table if exists genre_film;
drop table if exists suivi;
drop table if exists amis;
drop table if exists organisme;
drop table if exists personne;
drop table if exists utilisateur;

drop type if exists TYPE_P;
drop type if exists TYPE_O;
drop type if exists EMOJI;

CREATE TYPE TYPE_P AS ENUM ('acteur', 'realisateur', 'lambda');
CREATE TYPE TYPE_O AS ENUM ('cinema', 'studio', 'club', 'org_festival');
CREATE TYPE EMOJI AS ENUM ('coeur', 'jaime', 'jaime_pas', 'degout', 'joie', 'tristesse', 'peur', 'colere');

create table utilisateur (
    uid SERIAL PRIMARY KEY,
    pseudo VARCHAR(20) NOT NULL,
    mail VARCHAR(50) NOT NULL,
    photo VARCHAR,
    mot_de_passe VARCHAR(50) NOT NULL,
    description TEXT
);

create table personne (
    pid INTEGER PRIMARY KEY,
    prenom VARCHAR(30) NOT NULL,
    nom VARCHAR(30) NOT NULL,
    date_de_naissance DATE NOT NULL,
    nationalite VARCHAR,
    type_personne TYPE_P NOT NULL,
    FOREIGN KEY (pid) REFERENCES utilisateur(uid),
    CHECK (date_de_naissance < CURRENT_DATE)
);

create table organisme (
    oid INTEGER PRIMARY KEY,
    adresse VARCHAR,
    site_web VARCHAR,
    type_organisme TYPE_O NOT NULL,
    FOREIGN KEY (oid) REFERENCES utilisateur(uid)
);

create table amis (
    uid1 INTEGER,
    uid2 INTEGER,
    PRIMARY KEY (uid1,uid2),
    FOREIGN KEY (uid1) REFERENCES utilisateur(uid),
    FOREIGN KEY (uid2) REFERENCES utilisateur(uid),
    CHECK (uid1 <> uid2)
);

create table suivi (
    suiveur INTEGER,
    suivi INTEGER,
    PRIMARY KEY (suiveur,suivi),
    FOREIGN KEY (suiveur) REFERENCES utilisateur(uid),
    FOREIGN KEY (suivi) REFERENCES utilisateur(uid),
    CHECK (suiveur <> suivi)
);

create table genre_film (
    gid SERIAL PRIMARY KEY,
    nom VARCHAR NOT NULL
);

create table sous_genre (
    gid INTEGER,
    sgid SERIAL,
    PRIMARY KEY (gid, sgid),
    FOREIGN KEY (gid) REFERENCES genre_film(gid),
    FOREIGN KEY (sgid) REFERENCES genre_film(gid),
    CHECK (gid <> sgid)
);

create table film (
    fid SERIAL PRIMARY KEY,
    titre VARCHAR NOT NULL,
    id_genre INTEGER NOT NULL,
    annee_sortie INTEGER NOT NULL,
    duree TIME NOT NULL,
    FOREIGN KEY (id_genre) REFERENCES genre_film(gid),
    CHECK (annee_sortie <= EXTRACT (YEAR FROM CURRENT_DATE)),
    CHECK (duree > '00:00:00')
);

create table historique (
    uid INTEGER,
    fid INTEGER,
    PRIMARY KEY (uid, fid),
    FOREIGN KEY (uid) REFERENCES utilisateur(uid),
    FOREIGN KEY (fid) REFERENCES film(fid)
);

create table seance (
    oid INTEGER,
    fid INTEGER,
    date DATE,
    heure TIME,
    salle INTEGER,
    PRIMARY KEY (oid, date, heure, salle),
    FOREIGN KEY (oid) REFERENCES organisme(oid),
    FOREIGN KEY (fid) REFERENCES film(fid)
);

create table membre_club (
    oid INTEGER,
    pid INTEGER,
    PRIMARY KEY (oid, pid),
    FOREIGN KEY (oid) REFERENCES organisme(oid),
    FOREIGN KEY (pid) REFERENCES personne(pid)
);

create table produit (
    fid INTEGER PRIMARY KEY,
    oid INTEGER,
    nom_prod VARCHAR NOT NULL,
    FOREIGN KEY (fid) REFERENCES film(fid),
    FOREIGN KEY (oid) REFERENCES organisme(oid)
);

create table realise (
    fid INTEGER PRIMARY KEY,
    pid INTEGER,
    nom_real VARCHAR(30) NOT NULL,
    prenom_real VARCHAR(30),
    FOREIGN KEY (fid) REFERENCES film(fid),
    FOREIGN KEY (pid) REFERENCES personne(pid)
);

create table casting (
    pid INTEGER,
    fid INTEGER NOT NULL,
    role VARCHAR NOT NULL,
    prenom_acteur VARCHAR(30),
    nom_acteur VARCHAR(30) NOT NULL,
    PRIMARY KEY (fid, role),
    FOREIGN KEY (pid) REFERENCES personne(pid),
    FOREIGN KEY (fid) REFERENCES film(fid)
);

create table evenement (
    eid SERIAL PRIMARY KEY,
    titre VARCHAR NOT NULL,
    organisateur INTEGER NOT NULL,
    date DATE NOT NULL,
    prix INTEGER NOT NULL,
    lieu VARCHAR NOT NULL,
    places_total INTEGER,
    places_restantes INTEGER,
    FOREIGN KEY (organisateur) REFERENCES utilisateur(uid),
    CHECK (prix >= 0),
    CHECK (places_total > 0),
    CHECK (places_restantes >= 0)
);

create table even_passe (
    eid INTEGER PRIMARY KEY,
    nb_participants INTEGER,
    page_web VARCHAR,
    FOREIGN KEY (eid) REFERENCES evenement(eid),
    CHECK (nb_participants >= 0)
);

create table programme (
    eid INTEGER,
    fid INTEGER,
    heure TIME NOT NULL,
    PRIMARY KEY (eid, fid),
    FOREIGN KEY (eid) REFERENCES evenement(eid),
    FOREIGN KEY (fid) REFERENCES film(fid)
);

create table interet (
    pid INTEGER,
    eid INTEGER,
    participe BOOLEAN NOT NULL,
    PRIMARY KEY (eid, pid),
    FOREIGN KEY (eid) REFERENCES evenement(eid),
    FOREIGN KEY (pid) REFERENCES personne(pid)
);

create table publication (
    pubid SERIAL PRIMARY KEY,
    auteur INTEGER NOT NULL,
    date DATE NOT NULL,
    premier_niveau BOOLEAN NOT NULL,
    sujet VARCHAR NOT NULL,
    contenu TEXT NOT NULL,
    FOREIGN KEY (auteur) REFERENCES utilisateur(uid)
);

create table reponse (
    repid INTEGER,
    pubid INTEGER,
    PRIMARY KEY (repid, pubid),
    FOREIGN KEY (repid) REFERENCES publication(pubid),
    FOREIGN KEY (pubid) REFERENCES publication(pubid),
    CHECK (repid > pubid)
);

create table reaction (
    uid INTEGER, 
    pubid INTEGER,
    emoji EMOJI NOT NULL,
    PRIMARY KEY (uid, pubid),
    FOREIGN KEY (uid) REFERENCES utilisateur(uid),
    FOREIGN KEY (pubid) REFERENCES publication(pubid)
);

create table mots_cle (
    mid SERIAL PRIMARY KEY,
    mot VARCHAR NOT NULL,
    nb_utilisations INTEGER NOT NULL,
    CHECK (nb_utilisations > 0)
);

create table contient (
    pubid INTEGER,
    mid INTEGER,
    PRIMARY KEY (pubid, mid),
    FOREIGN KEY (pubid) REFERENCES publication(pubid),
    FOREIGN KEY (mid) REFERENCES mots_cle(mid)
);

\COPY utilisateur(pseudo,mail,photo,mot_de_passe,description) FROM 'fichiers_csv/user.csv' WITH (FORMAT csv, HEADER true);
\COPY personne(pid,prenom,nom,date_de_naissance,nationalite,type_personne) FROM 'fichiers_csv/personne.csv' WITH (FORMAT csv, HEADER true);
\COPY organisme(oid,adresse,site_web,type_organisme) FROM 'fichiers_csv/organisme.csv' WITH (FORMAT csv, HEADER true);
\COPY amis(uid1,uid2) FROM 'fichiers_csv/amis.csv' WITH (FORMAT csv, HEADER true);
\COPY suivi(suiveur,suivi) FROM 'fichiers_csv/suivi.csv' WITH (FORMAT csv, HEADER true);
\COPY genre_film(nom) FROM 'fichiers_csv/genre.csv' WITH (FORMAT csv, HEADER true);
\COPY sous_genre(gid,sgid) FROM 'fichiers_csv/sous_genre.csv' WITH (FORMAT csv, HEADER true);
\COPY film(titre,id_genre,annee_sortie,duree) FROM 'fichiers_csv/film.csv' WITH (FORMAT csv, HEADER true);
\COPY historique(uid,fid) FROM 'fichiers_csv/historique.csv' WITH (FORMAT csv, HEADER true);
\COPY seance(oid,fid,date,heure,salle) FROM 'fichiers_csv/seance.csv' WITH (FORMAT csv, HEADER true);
\COPY membre_club(pid,oid) FROM 'fichiers_csv/membres.csv' WITH (FORMAT csv, HEADER true);
\COPY produit(fid,oid,nom_prod) FROM 'fichiers_csv/produit.csv' WITH (FORMAT csv, HEADER true);
\COPY realise(fid,pid,nom_real,prenom_real) FROM 'fichiers_csv/realise.csv' WITH (FORMAT csv, HEADER true);
\COPY casting(pid,fid,role,prenom_acteur,nom_acteur) FROM 'fichiers_csv/casting.csv' WITH (FORMAT csv, HEADER true);
\COPY evenement(titre,organisateur,date,prix,lieu,places_total,places_restantes) FROM 'fichiers_csv/evenement.csv' WITH (FORMAT csv, HEADER true);
\COPY even_passe(eid,nb_participants,page_web) FROM 'fichiers_csv/even_passe.csv' WITH (FORMAT csv, HEADER true);
\COPY programme(eid,fid,heure) FROM 'fichiers_csv/programme.csv' WITH (FORMAT csv, HEADER true);
\COPY interet(pid,eid,participe) FROM 'fichiers_csv/interet.csv' WITH (FORMAT csv, HEADER true);
\COPY publication(auteur,date,premier_niveau,sujet,contenu) FROM 'fichiers_csv/publication.csv' WITH (FORMAT csv, HEADER true);
\COPY reponse(repid,pubid) FROM 'fichiers_csv/reponse.csv' WITH (FORMAT csv, HEADER true);
\COPY reaction(uid,pubid,emoji) FROM 'fichiers_csv/reaction.csv' WITH (FORMAT csv, HEADER true);
\COPY mots_cle(mot,nb_utilisations) FROM 'fichiers_csv/mots_cle.csv' WITH (FORMAT csv, HEADER true);
\COPY contient(pubid,mid) FROM 'fichiers_csv/contient.csv' WITH (FORMAT csv, HEADER true);