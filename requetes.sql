------------- 1) JOINTURE 3 TABLES -------------------
/* les publications qui contiennent le #Tarantino */
SELECT auteur, contenu
FROM publication
natural JOIN contient
natural JOIN mots_cle
WHERE mot LIKE 'Tarantino';

/* les films, leurs réalisateurs et leurs producteurs */
select titre, nom_real, nom_prod
from film 
natural join realise
natural join produit;
--------------------------------------------------------

-------------------- 2) AUTO JOINTURE ------------------
/* liste tous les amis d'un utilisateur */
PREPARE tous_les_amis(VARCHAR) AS
SELECT ami 
FROM
    (SELECT u1.pseudo AS utilisateur, u2.pseudo AS ami
    FROM utilisateur u1
    JOIN amis ON u1.uid = amis.uid1 OR u1.uid = amis.uid2
    JOIN utilisateur u2 ON amis.uid2 = u2.uid OR amis.uid1 = u2.uid
    WHERE u1.pseudo = $1) AS foo
WHERE utilisateur <> ami;
 -- EXECUTE tous_les_amis('mliz');
---------------------------------------------------------

------------ 3) une sous requete corrélée ----------------
--les films pour lesquels il n'y a pas de séances
select titre 
from film f 
where not exists ( 
    select * 
    from seance s
    where s.fid=f.fid);
----------------------------------------------------------

--------------------- 4) sous-requête FROM ----------------
/* les genres de film préférés d'un utilisateur */
PREPARE genre_prefere(VARCHAR) AS
SELECT nom, MAX(count) AS reccurence
FROM (SELECT id_genre, COUNT(*) AS count
      FROM historique
      NATURAL JOIN film
      WHERE uid = (SELECT uid FROM utilisateur WHERE pseudo = $1)
      GROUP BY id_genre) AS foo
JOIN genre_film ON foo.id_genre = genre_film.gid
JOIN film ON foo.id_genre = film.id_genre
JOIN historique ON film.fid = historique.fid
GROUP BY nom, historique.uid
HAVING historique.uid = (SELECT uid FROM utilisateur WHERE pseudo = $1)
ORDER BY reccurence DESC;
-- EXECUTE genre_prefere('egoia');
------------------------------------------------------------

----------------------- 6) GROUP BY et HAVING ----------------
/* les événements ayant vendu moins de 50% des places */
SELECT e.titre AS evenement, e.places_total, e.places_restantes
FROM evenement e
GROUP BY e.eid, e.titre, e.places_total, e.places_restantes
HAVING e.places_restantes >= (e.places_total/2)
ORDER BY places_restantes DESC;

/* les utilisateurs de type réalisateurs 
    ayant réalisé au moins 2 films présents dans la bdd */
SELECT r.prenom_real || ' ' || r.nom_real AS realisateur, COUNT(*) AS nb_films_realises
FROM realise r
WHERE r.pid IS NOT NULL
GROUP BY r.pid, realisateur
HAVING COUNT(*) > 1;
-----------------------------------------------------------

------------- 7) calcul de deux agrégats --------------------
/* pour chaque club le nombre moyen de film que ses membres ont vu */
select pseudo, AVG(tot_film)
from (utilisateur u join organisme o on o.oid=u.uid)
join (
    select * from membre_club m join (
        select uid, COUNT(fid) AS tot_film
        from historique
        GROUP BY uid) AS h
    on m.pid=h.uid) AS m1
on m1.oid = o.oid
where type_organisme = 'club'
GROUP BY u.pseudo;
------------------------------------------------------------

----------------------- 8) jointure externe ----------------
/* liste de tous les films avec leur réalisateur 
    (même si ce dernier n'existe pas dans la table "realise")*/
SELECT titre, nom_real AS realisateur
FROM film F
NATURAL LEFT JOIN realise;

/* liste tous les films avec leurs acteurs (3 principaux) */
SELECT titre, prenom_acteur || ' ' || nom_acteur AS acteur
FROM film
NATURAL FULL JOIN casting
ORDER BY titre;
------------------------------------------------------------

-------- 9) condition de totalité -------------------------
/* les films qui passent dans tous les cinemas */
-- requete 1 sous requêtes corrélées
select titre
from film f 
where not exists (
    select *
    from organisme c
    where type_organisme='cinema' and not exists(
        select *
        from seance s
        where s.fid = f.fid and s.oid=c.oid
    ));

-- requete 2 avec de l'agrégation
select titre 
from film natural join seance
group by titre
having count(distinct oid)=(
    select COUNT(oid) 
    from organisme
    where type_organisme='cinema');
------------------------------------------------------------

----------------------- 10) MEME RESULTAT -------------------
/* la liste des films et de leurs producteurs */
-- Premiere requete
SELECT titre, nom_prod AS producteur
FROM produit
NATURAL RIGHT JOIN film;
-- Seconde requete
SELECT titre, nom_prod AS producteur
FROM produit
NATURAL JOIN film;

-- Correction de la premiere
SELECT titre, nom_prod AS producteur
FROM produit
NATURAL RIGHT JOIN film
WHERE nom_prod IS NOT NULL;
------------------------------------------------------------

------------------------ 11) RECURSIVITE ---------------------
--certains amis d'amis de l'utilisateur $1
PREPARE amis_damis(INTEGER) AS
WITH RECURSIVE amis_damis1 AS(
    SELECT uid1 as uid, 0 as profondeur from amis where uid1=$1
    UNION ALL
    SELECT uid2 as uid, profondeur+1 from amis_damis1, amis where uid=uid1
),
amis_damis2 AS(
    SELECT * FROM amis_damis1
    UNION ALL
    SELECT uid1 as uid, profondeur+1 from amis_damis2, amis where uid=uid2
)
SELECT uid, MIN(profondeur) as shorter_path
from amis_damis2 
group by uid
order by shorter_path;
-- EXECUTE amis_damis(11);

/* conversations engendrées par la publication $1 */
PREPARE conversations(INTEGER) AS
WITH RECURSIVE conversation AS(
    SELECT pubid , $1||'' as fil from reponse where pubid=$1
    UNION ALL
    SELECT r.repid as pubid, fil||' -> '||r.repid from conversation c natural join reponse r where c.pubid=r.pubid
)
SELECT distinct * from conversation order by pubid;
-- EXECUTE conversations(4);
-------------------------------------------------------------

------------------------- 12) fenêtrage --------------------
/* classement des films en fonction du nombre
    d'utilisateur de CineNet l'ayant vu */
SELECT titre, nb_vues, classement
FROM (
    SELECT titre, COUNT(*) AS nb_vues,
        DENSE_RANK() OVER (ORDER BY COUNT(*) DESC) AS classement
    FROM film
    NATURAL JOIN historique
    GROUP BY titre
) AS foo;
-------------------------------------------------------------

----------- AUTRES REQUETES ---------------------------------

/* le % de remplissage de places pour le total des festivals organisés
    pour chaque utilisateur ayant le type "org_festival" */

-- SANS fenêtrage
WITH total_remplissage AS (
    WITH pcent_rempli AS 
        (SELECT DISTINCT U.uid, (EP.nb_participants*100/E.places_total) AS pourcentage
        FROM utilisateur U
        JOIN evenement E ON E.organisateur = U.uid
        NATURAL JOIN even_passe EP)
    SELECT uid, ROUND(AVG(pourcentage)) AS en_pourcent
    FROM pcent_rempli
    GROUP BY uid
)
SELECT U.pseudo AS organisateur, TR.en_pourcent AS taux_remplissage
FROM utilisateur U 
NATURAL JOIN total_remplissage TR
JOIN organisme O ON U.uid = O.oid
WHERE O.type_organisme = 'org_festival';

-- AVEC fenêtrage
WITH pcent_rempli AS (
    SELECT U.uid, E.eid,
        (EP.nb_participants * 100 / E.places_total) AS pourcentage,
        AVG(EP.nb_participants * 100 / E.places_total) OVER (PARTITION BY E.eid) AS moyenne_pourcentage
    FROM utilisateur U
    JOIN evenement E ON E.organisateur = U.uid
    JOIN even_passe EP ON E.eid = EP.eid
)
SELECT U.pseudo AS organisateur, ROUND(AVG(pr.moyenne_pourcentage)) AS taux_remplissage
FROM utilisateur U
JOIN pcent_rempli pr ON U.uid = pr.uid
JOIN organisme O ON U.uid = O.oid
WHERE O.type_organisme = 'org_festival'
GROUP BY U.uid, U.pseudo;

/* les utilisateurs ayant vu les memes films qu'un certain utilisateur */
PREPARE films_en_commun(VARCHAR) AS
SELECT u2.pseudo, f.titre AS film_commun
FROM utilisateur u1
JOIN historique h1 ON u1.uid = h1.uid
JOIN film f ON h1.fid = f.fid
JOIN historique h2 ON h1.fid = h2.fid AND h1.uid <> h2.uid
JOIN utilisateur u2 ON h2.uid = u2.uid
WHERE u1.pseudo = $1;
 -- EXECUTE films_en_commun('mliz');

/* les X utilisateurs les plus actifs */
PREPARE utilisateurs_actifs(INTEGER) AS
WITH reactions AS 
    (SELECT uid, COUNT(pubid) AS nb_reac
    FROM reaction
    GROUP BY uid
    ORDER BY nb_reac DESC),
publications AS 
    (SELECT auteur, COUNT(pubid) AS nb_publi
    FROM publication
    GROUP BY auteur
    ORDER BY nb_publi DESC)
SELECT pseudo, SUM(nb_reac+nb_publi) AS interactions
FROM reactions
JOIN publications ON auteur = uid
NATURAL JOIN utilisateur
GROUP BY pseudo
ORDER BY interactions DESC
LIMIT $1;
-- EXECUTE utilisateurs_actifs(10);

/* les couples de publications 
ayant un mot clé en commun */
SELECT C2.pubid, C1.pubid, M.mot
FROM contient C1, contient C2
NATURAL JOIN mots_cle M 
WHERE C1.mid = C2.mid
AND C1.pubid <> C2.pubid
AND M.mid = C1.mid;

/* liste des films qui sont un genre ou un sous genre de romance */
PREPARE films_par_genre(VARCHAR) AS
SELECT F.titre, G.nom AS genre
FROM film F 
JOIN genre_film G ON G.gid = F.id_genre
WHERE G.nom = $1
OR G.gid IN 
    (SELECT SG.sgid 
    FROM sous_genre SG 
    WHERE gid = (SELECT G2.gid 
                FROM genre_film G2 
                WHERE G2.nom = $1))
ORDER BY G.nom;
-- EXECUTE films_par_genre('Romance');

/* nombre d'abonnés, d'abonnements et d'amis de chaque utilisateur */
WITH amities AS 
    (SELECT p1_pseudo, 0+COUNT(p2_pseudo) AS nb_amis
    FROM (SELECT U1.pseudo AS p1_pseudo, U2.pseudo AS p2_pseudo
        FROM utilisateur U1, utilisateur U2
        LEFT JOIN amis A ON A.uid1 = U2.uid
        WHERE A.uid2 = U1.uid
        UNION
        SELECT U1.pseudo AS p1_pseudo, U2.pseudo AS p2_pseudo
        FROM utilisateur U1, utilisateur U2
        LEFT JOIN amis A ON A.uid2 = U2.uid
        WHERE A.uid1 = U1.uid) AS foo
    GROUP BY p1_pseudo),
abonnes AS
    (SELECT p1_pseudo, COUNT(p2_pseudo) AS nb_abonnes
    FROM (SELECT U1.pseudo AS p1_pseudo, U2.pseudo AS p2_pseudo
        FROM utilisateur U1, utilisateur U2
        LEFT JOIN suivi A ON A.suiveur = U2.uid
        WHERE A.suivi = U1.uid) AS foo
    GROUP BY p1_pseudo),
abonnements AS 
    (SELECT p1_pseudo, COUNT(p2_pseudo) AS nb_abonnements
    FROM (SELECT U1.pseudo AS p1_pseudo, U2.pseudo AS p2_pseudo
        FROM utilisateur U1, utilisateur U2
        LEFT JOIN suivi A ON A.suivi = U2.uid
        WHERE A.suiveur = U1.uid) AS foo
    GROUP BY p1_pseudo)
SELECT U.pseudo, 
       COALESCE(nb_amis, 0) AS nb_amis, -- toute valeur null est remplacée par 0
       COALESCE(nb_abonnes, 0) AS nb_abonnes, 
       COALESCE(nb_abonnements, 0) AS nb_abonnements
FROM utilisateur U 
LEFT JOIN amities ON U.pseudo = amities.p1_pseudo
LEFT JOIN abonnes ON U.pseudo = abonnes.p1_pseudo
LEFT JOIN abonnements ON U.pseudo = abonnements.p1_pseudo;

/* nombre de réactions et de réponses à chaque publication */
WITH total_reactions AS (
    SELECT pubid AS publication, COUNT(emoji) AS reactions
    FROM reaction
    GROUP BY publication
),
total_reponses AS (
    SELECT pubid AS publication, COUNT(repid) AS reponses
    FROM reponse
    GROUP BY pubid
)
SELECT COALESCE(R1.publication, R2.publication) AS publication, 
       COALESCE(reponses,0) AS nb_reponses, 
       COALESCE(reactions,0) AS nb_reactions
FROM total_reponses R1
NATURAL FULL JOIN total_reactions R2
ORDER BY (reponses+reactions) DESC;

/* nombre de réactions par type d'émoji à une publication */
PREPARE emojis_publication(INTEGER) AS
SELECT emoji, COUNT(uid) AS reccurence
FROM reaction
WHERE pubid = $1
GROUP BY(emoji)
ORDER BY reccurence DESC;
-- EXECUTE emojis_publication(1);

/* liste des X dernières publications du site */
PREPARE publications_recentes(INTEGER) AS
SELECT auteur, pubid, date
FROM publication
ORDER BY date DESC
LIMIT $1;
-- EXECUTE publications_recentes(5);

/* liste de toutes les séances d'un cinéma */
PREPARE seances_cinema(INTEGER) AS
SELECT U.pseudo, F.titre, S.date, S.heure, S.salle
FROM seance S 
NATURAL JOIN film F 
NATURAL JOIN organisme O 
JOIN utilisateur U ON U.uid = O.oid
WHERE O.oid = $1
ORDER BY (date,heure);
-- EXECUTE seances_cinema(24);

/* paires de publications qui ont un mot clé en commun */
SELECT C1.pubid AS pub1, C2.pubid AS pub2
FROM contient C1
JOIN contient C2 ON C1.mid = C2.mid
WHERE C1.pubid <> C2.pubid;
