## PROJET BDD CINENET

## GROUPE 55 
    - Melissa Bouloufa
    - Elyas Saidoune

## CREATION ET PEUPLEMENT DES TABLES
    \i cinenet.sql

## REQUETES 
    \i requetes.sql

    ¤ les requêtes se trouvent dans le fichier requetes.sql
    Certaines requêtes sont paramétrées et doivent être testées après l'exécution du fichier

    - EXECUTE genre_prefere(pseudo);  (ex. pseudo = 'egoia')
    - EXECUTE amis_damis(uid);  (ex. uid = 2)
    - EXECUTE conversations(pubid); (ex. pubid = 4)
    - EXECUTE films_en_commun(pseudo); (ex. pseudo = 'mliz')
    - EXECUTE utilisateurs_actifs(n); 
    - EXECUTE films_par_genre(genre); (ex. genre = 'Romance')


## RECOMMANDATION
    \i reco_event.sql
    \i reco_film.sql
    \i reco_users.sql
    \i reco_publi.sql

    Ensuite : 
    - EXECUTE reco_event(uid); (ex. uid = 2)
    - EXECUTE reco_film(uid); 
    - EXECUTE reco_users(uid);
    - EXECUTE reco_publi(uid);

    ¤ les fichiers reco_event.sql, reco_film.sql, reco_users.sql et
      reco_publi.sql contiennent les différentes requetes parametrées pour la recommandation.

## AUTRES FICHIERS
    - diagramme.pdf contient le diagramme
    - rapport.pdf contient le rapport avec des explications détaillées sur l'algorithme de recommandation
    - fichiers .csv contenus dans le répértoire fichier_csv pour le peuplement des tables